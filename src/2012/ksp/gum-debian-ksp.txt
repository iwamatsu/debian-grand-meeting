                    Nobuhiro Iwamatsu <iwamatsu@debian.org>

D E B I A N   G R A N D   U N I F I C A T I O N   M E E T I N G   K E Y S I G N I N G

                           List of Participants  (v 1.0)


Here's what you have to do with this file:

1. Print this file to paper.
2. Compute this file's SHA256 checksum: gum-debian-ksp.txt
3. fill in the hash values on the printout.
4. Bring the printout, a pen, and proof of identity to the keysigning.

For each participant:

1. Compare the hash you computed with the other participant.
2. Ask if the other participant's gpg fingerprint on the hardcopy is correct.
3. Verify each other's identity by checking preferably a passport.
4. If you are satisfied with the identification, mark on your hardcopy that
   the other participant's gpg fingerprint is correct and has been identified.


SHA256 Checksum: _________________________________________________________ [ ]


#1  HIGUCHI Daisuke (VDR dai)

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/D439668E 2010-03-26
      Key fingerprint = 0B29 D88E 42E6 B765 B8D8  EA50 7839 619D D439 668E
uid                  HIGUCHI Daisuke (VDR dai) <dai@debian.org>
uid                  VDR dai (d) <d+d@vdr.jp>
uid                  VDR dai (deb) <d+deb@vdr.jp>
uid                  VDR dai (bugzilla) <d+bugzilla@vdr.jp>
uid                  HIGUCHI Daisuke (VDR dai) <debian@vdr.jp>
sub   4096R/024C769F 2010-03-26



#2  Koichi Akabe

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/9C840E81 2011-08-23
      Key fingerprint = 281C 6E4D 93EF F746 CAA9  C2E8 E402 1529 9C84 0E81
uid                  Koichi Akabe <vbkaisetsu@gmail.com>
uid                  Koichi Akabe <vbkaisetsu@debian.or.jp>
sub   4096R/BECF85FA 2011-08-23



#3  Hiroyuki Yamamoto

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/520304DC 2009-09-04
      Key fingerprint = A75D B285 7050 4BF9 AEDA  91AC 3A10 59C6 5203 04DC
uid                  Hiroyuki Yamamoto <yama1066@gmail.com>
uid                  Hiroyuki Yamamoto (Debian JP Project) <yamamoto@debian.or.jp>
sub   4096R/824BFFBF 2009-09-04



#4  KURASHIKI Satoru

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/40A2F113 2009-09-10
      Key fingerprint = C11C A58C 0175 C240 D58C  30C8 D27D DE11 40A2 F113
uid                  KURASHIKI Satoru <lurdan@gmail.com>
uid                  KURASHIKI Satoru <lurdan@debian.or.jp>
sub   4096R/D7B08969 2009-09-10



#5  NAKAO Keisuke

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/D4D44911 2009-10-30
      Key fingerprint = 391D 3F37 D176 3CE9 BC8A  59AF E0F5 EEEA D4D4 4911
uid                  NAKAO Keisuke <chome@argv.org>
sub   4096R/E7AB3204 2009-10-30



#6  Yusuke YATSUO

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/704B0F66 2012-03-23
      Key fingerprint = A450 6EDE 0C7B 18F3 12EA  A15C 0198 9F56 704B 0F66
uid                  Yusuke YATSUO <yyatsuo@gmail.com>
uid                  Yusuke YATSUO <yusuke@yyatsuo.com>
sub   4096R/55B93724 2012-03-23



#7  Tetsutaro KAWADA

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/AD40A2BC 2009-11-06
      Key fingerprint = 0D1A EBF6 DA09 8A7B D9A4  459A 8B0D FCCB AD40 A2BC
uid                  Tetsutaro KAWADA <t3rkwd@gmail.com>
uid                  KAWADA Tetsutaro <t3rkwd@gmail.com>
uid                  Tetsutaro KAWADA <t3rkwd@debian.or.jp>
sub   4096R/002CF331 2009-11-06



#8  Nobuhiro Iwamatsu

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/40AD1FA6 2009-06-25
      Key fingerprint = 5E62 9EE5 2321 9735 7B84  CF43 3224 7FBB 40AD 1FA6
uid                  Nobuhiro Iwamatsu <iwamatsu@debian.org>
uid                  Nobuhiro Iwamatsu <hemamu@t-base.ne.jp>
uid                  Nobuhiro Iwamatsu <iwamatsu@nigauri.org>
uid                  Nobuhiro Iwamatsu <iwamatsu@debian.or.jp>
sub   4096R/35468237 2009-06-25



#9  Osamu Matsumoto

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/445B74F2 2009-11-02
      Key fingerprint = 5A3E 64E0 D914 C77F 164B  B107 153F B67E 445B 74F2
uid                  Osamu Matsumoto <osamu@osamu-m.org>
uid                  Osamu Matsumoto <osamu.matsumoto@gmail.com>
uid                  Osamu Matsumoto <osamu@users.sourceforge.jp>
sub   4096R/B624B77F 2009-11-02



#10  Youhei SASAKI

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/891D7E07 2009-09-03
      Key fingerprint = 66A4 EA70 4FE2 4055 8D6A  C2E6 9394 F354 891D 7E07
uid                  Youhei SASAKI <uwabami@gfd-dennou.org>
uid                  Youhei SASAKI (JunkHub) <uwabami@junkhub.org>
uid                  Youhei SASAKI (Debian JP Project) <uwabami@debian.or.jp>
sub   4096R/9DBFD29F 2009-09-03



#11  Kouhei Sutou

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/079F8007 2010-08-30
      Key fingerprint = 08D3 564B 7C6A 9CAF BFF6  A667 91D1 8FCF 079F 8007
uid                  Kouhei Sutou <kou@clear-code.com>
uid                  Kouhei Sutou <kou@cozmixng.org>
sub   4096R/CC0826F5 2010-08-30



#12  SUGIMOTO Norimitsu

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/1AAB9619 2009-11-08
      Key fingerprint = 20B6 3538 C8BE 6373 9E07  B5BA 3F0A 115F 1AAB 9619
uid                  SUGIMOTO Norimitsu <dictoss@live.jp>
sub   4096R/968BFE8A 2011-11-21



#13  YOSHIDA Tomohiro

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/D5FDB94B 2011-04-14
      Key fingerprint = 5D0D 8C35 87FE 77ED D6BB  9E07 9BB0 CFAE D5FD B94B
uid                  YOSHIDA Tomohiro <YOSHIDA.TOMO.Tomohiro@gmail.com>
uid                  YOSHIDA Tomohiro <tomohiro@sep.email.ne.jp>
sub   4096R/F587ED39 2011-04-28



#14  NOKUBI Takatsugu

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   1024D/C2CE8099 1999-11-01
      Key fingerprint = BDD7 23BB B724 1FA2 1BB3  2EC0 2BA8 2602 C2CE 8099
uid                  NOKUBI Takatsugu <knok@daionet.gr.jp>
uid                  NOKUBI Takatsugu <knok@fsij.org>
uid                  NOKUBI Takatsugu <knok@debian.or.jp>
uid                  NOKUBI Takatsugu (Debian Project) <knok@debian.org>
uid                  NOKUBI Takatsugu (Namazu Project) <knok@namazu.org>
uid                  NOKUBI Takatsugu (Office) <takatsugu.nokubi@toppan.co.jp>
sub   1024g/B5BAAAE8 1999-11-01



#15  Hideki Yamane (Debian-JP)

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/2AAAB140 2009-05-23 [expires: 2019-05-21]
      Key fingerprint = 58E1 222F 9696 C885 A3CD  104C 5D32 8D08 2AAA B140
uid                  Hideki Yamane (Debian-JP) <henrich@debian.or.jp>
uid                  Hideki Yamane <henrich@debian.org>
uid                  Hideki Yamane (private) <henrich@iijmio-mail.jp>
sub   4096R/108820E1 2009-05-23 [expires: 2019-05-21]



#16  Daigo Moriwaki

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/F28A0D3E 2009-09-04 [expires: 2019-09-02]
      Key fingerprint = 54D0 AC54 DBBD 3F21 18B0  9CCE 01CB CC4B F28A 0D3E
uid                  Daigo Moriwaki <beatles@sgtpepper.net>
uid                  Daigo Moriwaki <daigo@debian.org>
sub   4096R/02CE7FC9 2009-09-04 [expires: 2019-09-02]



#17  Kouhei Maeda

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/7E37CE41 2009-06-19
      Key fingerprint = B8F2 86F6 2206 360F 7D41  0817 2E81 6254 7E37 CE41
uid                  Kouhei Maeda <mkouhei@palmtb.net>
uid                  Kouhei Maeda <mkouhei@gmail.com>
uid                  Kouhei Maeda (Debian JP Project) <mkouhei@debian.or.jp>
sub   4096R/35992CC1 2009-06-19



#18  Jiro Matsuzawa

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/ECC442E9 2010-09-29
      Key fingerprint = E086 C14A 869F BB0E 3541  19EB E370 B08B ECC4 42E9
uid                  Jiro Matsuzawa <jmatsuzawa@gnome.org>
uid                  Jiro Matsuzawa <matsuzawa.jr@gmail.com>
uid                  Jiro Matsuzawa <jmatsuzawa@src.gnome.org>
sub   4096R/06080AE1 2010-09-29



#19  YAMADA Tsuyoshi

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/3184B100 2012-06-16
      Key fingerprint = E82F C5AF 51B4 F2F1 8F5C  9BFD D158 7330 3184 B100
uid                  YAMADA Tsuyoshi <tyamada@minimum2scp.org>
sub   4096R/4048E07C 2012-06-16



#20  Taku YASUI

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/E909CDEE 2009-09-10
      Key fingerprint = 9476 9E52 D436 D58B 260C  A7DD 40D7 16CF E909 CDEE
uid                  Taku YASUI <tach@debian.org>
uid                  Taku YASUI <tach@osdn.jp>
uid                  Taku YASUI <tach@arege.jp>
uid                  Taku YASUI <tach@arege.net>
uid                  Taku YASUI <tach@slashdot.jp>
uid                  Taku YASUI <tach@debian.or.jp>
uid                  Taku YASUI <tach@sourceforge.jp>
uid                  Taku YASUI <taku.yasui@gmail.com>
uid                  Taku YASUI <taku.yasui@facebook.com>
uid                  Taku YASUI <taku.yasui@mail.rakuten.com>
uid                  Taku YASUI <taku.yasui@mail.rakuten.co.jp>
sub   4096R/7AC0CE03 2009-09-10



#21  TANIGUCHI Takaki

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/28A5E257 2009-09-27
      Key fingerprint = D24A BFD1 27CD 2556 A13C  6C79 CC14 9F6C 28A5 E257
uid                  TANIGUCHI Takaki <takaki@media-as.org>
uid                  TANIGUCHI Takaki <takaki@debian.org>
uid                  TANIGUCHI Takaki <takaki@nagoya-u.jp>
uid                  TANIGUCHI Takaki <takaki@debian.or.jp>
uid                  TANIGUCHI Takaki <takaki@asis.media-as.org>
uid                  TANIGUCHI Takaki <takaki@takaki.media-as.org>
uid                  TANIGUCHI Takaki <taniguchi.takaki@gmail.com>
uid                  TANIGUCHI Takaki <takaki@users.sourceforge.jp>
uid                  TANIGUCHI Takaki <takaki@users.sourceforge.net>
sub   4096R/FB38290C 2009-09-27



#22  YOSHINO Yoshihito

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/C40765C1 2009-12-19
      Key fingerprint = DCF6 B5C3 E9FF C438 DBEE  95D7 259D BF32 C407 65C1
uid                  YOSHINO Yoshihito <yy.y.ja.jp@gmail.com>
sub   4096R/BC59023B 2009-12-19



#23  Kubo, Hiroshi (gdevmjc, of Chestnut Valley)

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/86FB366B 2011-07-31
      Key fingerprint = F496 6001 976F FF24 6D7D  C774 9248 AD34 86FB 366B
uid                  Kubo, Hiroshi (gdevmjc, of Chestnut Valley) <h-kubo@geisya.or.jp>
sub   4096R/C3F570E3 2011-07-31



#24  Tadaki SAKAI

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/1782EA7E 2012-03-24
      Key fingerprint = 9F8D 7799 4737 7FCD 4759  6A1D 105B 030D 1782 EA7E
uid                  Tadaki SAKAI <stadaki.dev@gmail.com>
sub   4096R/94C70B19 2012-03-24



#25  Takahide Nojima

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/52780494 2011-02-27
      Key fingerprint = E5C7 FFDB F824 8A9A FF5F  D84B 7F6C 594E 5278 0494
uid                  Takahide Nojima <nozzy123nozzy@gmail.com>
sub   4096R/A5F0BA62 2011-02-27



#26  Satoshi FURUNO

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/5A2A375B 2010-12-17
      Key fingerprint = CC8B 7D50 4CF1 11A4 CC6B  C636 F751 430D 5A2A 375B
uid                  Satoshi FURUNO <furunos@furunos.com>
sub   4096R/8D65BBA8 2010-12-17



#27  Akihiro Terasaki

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/6D0556E0 2012-06-20
      Key fingerprint = EC42 4E6F A049 3C19 D8D2  5AAA E717 F68D 6D05 56E0
uid                  Akihiro Terasaki <aki@thanku.jp>
sub   4096R/FA11EF64 2012-06-20



#28  Keita Maehara

          [ ] Fingerprint(s) OK        [ ] ID OK

pub   4096R/FEAB6467 2009-12-26
      Key fingerprint = 9B19 1E86 1777 2B2C 9A66  1200 4EA5 2161 FEAB 6467
uid                  Keita Maehara <maehara@debian.org>
sub   4096R/322A2A5B 2009-12-26



