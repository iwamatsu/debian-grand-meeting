#!/bin/sh

LANG=ja_JP.EUC-JP
export LANG

VERSION="stable"

TEMPFILE="include/mirrors"

MIRRORLIST=MirrorsJP.list
APTSOURCE=apt/sources.list.

rm -f $TEMPFILE

echo "<dl>" >> $TEMPFILE

NEST=0
LANGUAGE=ja

while read LINE
  do
  PARAMETER=$(echo $LINE | sed -e "s/:.*//")
  VALUE=$(echo $LINE | sed -e "s/.*: //")
  
  if test "$(echo $LINE | sed -e 's/\(.\).*/\1/')" = "#"
      then
      continue
  fi
  
  if test -z $PARAMETER
      then
      continue
  fi
  
  if test $PARAMETER = "Name.$LANGUAGE"
      then
      NAME=$VALUE
  elif test $PARAMETER = "URI"
      then
      URI=$VALUE
  elif test $PARAMETER = "Site"
      then
      SITE=$VALUE
      SOURCE=$(echo $SITE | sed -e "s#://#.#" -e "s#/.*##")
  elif test $PARAMETER = "<dl>"
      then
      echo >> $TEMPFILE
      echo "<dt><a href=\"${URI}\">$NAME</a></dt>" >> $TEMPFILE
      echo "<dd>" >> $TEMPFILE
      echo " <dl>" >> $TEMPFILE
      NAME=""
      URI=""
      NEST=1
      continue
  elif test $PARAMETER = "</dl>"
      then
      echo " </dl>" >> $TEMPFILE
      NEST=0
      continue
  else
      continue
  fi
  
  if test $NEST = "0" && test -n "$NAME" && test -n "$URI" && test -n "$SITE"
      then
      echo >> $TEMPFILE
      echo "<dt><a href=\"${URI}\">$NAME</a></dt>" >> $TEMPFILE
      echo "<dd><a href=\"${SITE}\">${SITE}</a>&nbsp;<small>[<a href=\"$APTSOURCE$SOURCE\">サンプル</a>]</small></dd>" >> $TEMPFILE
      NAME=""
      URI=""
      SITE=""
      SOURCE=""
      continue
  fi
  
  if test $NEST = "0" && test -n "$SITE" && test -z "$NAME" && test -z "$URI"
      then
      echo "<dd><a href=\"${SITE}\">${SITE}</a>&nbsp;<small>[<a href=\"$APTSOURCE$SOURCE\">サンプル</a>]</small></dd>" >> $TEMPFILE
      SITE=""
      SOURCE=""
      continue
  fi
  
  if test $NEST = "1" && test -n "$NAME" && test -n "$SITE"
      then
      echo >> $TEMPFILE
      echo " <dt>$NAME</dt>" >> $TEMPFILE
      echo " <dd><a href=\"${SITE}\">${SITE}</a>&nbsp;<small>[<a href=\"$APTSOURCE$SOURCE\">サンプル</a>]</small></dd>" >> $TEMPFILE
      NAME=""
      SITE=""
      SOURCE=""
      continue
  fi
  
  if test $NEST = "1" && test -n "$SITE" && test -z "$NAME" && test -z "$URI"
      then
      echo " <dd><a href=\"${SITE}\">${SITE}</a>&nbsp;<small>[<a href=\"$APTSOURCE$SOURCE\">サンプル</a>]</small></dd>" >> $TEMPFILE
      SITE=""
      SOURCE=""
      continue
  fi
done < $MIRRORLIST

echo "</dl>" >> $TEMPFILE
