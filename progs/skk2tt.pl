#!/usr/bin/perl
# -*- mode: perl -*-
# Copyright (C) 1998, Mitsuru Oka <oka@debian.or.jp>
# Modify for Template Toolkit by 2010, KURASAWA Nozomu <nabetaro@debian.or.jp>

%words = {};                    # insert a,b,c,...,z,etc.. of array

# 擬似 skk フォーマットをパース、中間テーブル構築
while(<>) {
    next if /^(;.*|)$/;
    m|([a-zA-Z][a-zA-Z0-9-.\s]*)\s+(/.+/)$| || die "error `$_'";
    my $keyword = $1;
    my $key = uc(substr($keyword,0,1));
    my $transwords = $2;

    if ($word{$key}) {
        $word{$key}->{$keyword} = $transwords;
    } else {
        $word{$key} = {};
        $word{$key}{$keyword} = $transwords;
    }
}

$current_time = localtime time; # 更新日付

# TT コード生成ヘッダ部分
print <<"EOL";
[% title = "Debian JP Doc/WWW 対訳表" %]
<h2>Debian JP Doc/WWW 対訳表</h2>
<p>
これは Debian JP Doc/WWW Project において訳語統一の指針として
扱っている対訳表です。
</p>

EOL

# TT コード生成部
foreach (sort keys %word) {
    my $key = $_;

    print <<"EOL";
<!-- ---------------------------------------------------------------- -->
    <h2> [$_] </h2>
<!-- ---------------------------------------------------------------- -->
      <dl>
EOL
    my $c_word = $word{$key};
    foreach (sort keys %$c_word) {
        my $keyword = $_;
        print <<"EOL";
        <dt>$keyword</dt><dd>$c_word->{$keyword}</dd>
EOL
    }
    print <<EOL;
      </dl>
EOL
}
